run: build
	./bin/simulator

build: vendor docs
	go build -o bin/simulator ./cmd/

docs:
	cd vendor/github.com/swaggo/swag/cmd/swag && go install .
	swag init --generalInfo cmd/main.go

vendor:
	dep ensure

clean:
	rm -rf vendor docs bin
