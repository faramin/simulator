# Game Simulator

## Contents
- [Build](#build)
  - [Prerequisites](#prerequisites)
  - [Building with make](#building-with-make)
  - [Manually building](#manually-building)
- [Usage](#usage)

## Build

### Prerequisites
 - [Go](https://golang.org/)
 - [dep](https://golang.github.io/dep/)
 - make

### Building with make

The easiest way to build the command line package is to use make. Run the following command in a terminal in the project directory:
```
$ make build
```

This creates an executable binary in `bin` directory in project directory.

Note: You might need sudo privileges to run the above command, since it installs [swag](https://github.com/swaggo/swag/) cli.

### Manually building
Just run the commands (lines with a tab indent) found in Makefile in a terminal in the project directory. :)

## Usage

```
  simulator <bind>
```
